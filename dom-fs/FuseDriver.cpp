// file      : dom-fs/FuseDriver.cpp
// copyright : Copyright (c) 2019 Alexander Gnatyuk
// license   : GPLv2; see accompanying LICENSE file

#include <cerrno>

module FuseDriver;

import std.core;
import Fuse;
import FileSystem;
import Wrappers;
import DomParser;

FileSystem fs;

void* init(fuse_conn_info* /*conn*/, fuse_config* cfg) {
    cfg->kernel_cache = 1;
    return nullptr;
}

int getattr(char const* path, struct stat* stbuf, fuse_file_info* /*fi*/) {
    memset_w(stbuf, 0, sizeof(struct stat));
    if (fs.directoryExists(path)) {
        stbuf->st_mode = FileTypes::Directory | 0755;
        stbuf->st_nlink = 2;
    } else if (auto const contents = fs.fileContents(path);
               contents.has_value()) {
        stbuf->st_mode = FileTypes::RegularFile | 0444;
        stbuf->st_nlink = 1;
        stbuf->st_size = (*contents).size();
    } else {
        return -ENOENT;
    }

    return 0;
}

int readdir(char const* path, void* buf, fuse_fill_dir_t filler,
            off_t /*offset*/, fuse_file_info* /*fi*/,
            fuse_readdir_flags /*flags*/) {
    auto const dirContents = fs.directoryContents(path);
    if (!dirContents.has_value()) {
        return -ENOENT;
    }

    auto f = fuse_fill_dir_flags(0);
    filler(buf, ".", nullptr, 0, f);
    filler(buf, "..", nullptr, 0, f);
    auto const& myContents = *dirContents;
    for (std::string const& str : myContents) {
        filler(buf, str.c_str(), nullptr, 0, f);
    }

    return 0;
}

int open(char const* path, fuse_file_info* fi) {
    if (!fs.fileExists(path))
        return -ENOENT;

    if ((fi->flags & Open::AccMode) != Open::ReadOnly)
        return -EACCES;

    return 0;
}

int read(char const* path, char* buf, size_t size, off_t offset,
         fuse_file_info* /*fi*/) {
    auto const contents = fs.fileContents(path);
    if (!contents.has_value()) {
        return -ENOENT;
    }

    auto const len = (*contents).size();
    if (offset >= len) {
        return 0;
    }
    if (offset + size > len) {
        size = len - offset;
    }
    memcpy_w(buf, (*contents).c_str() + offset, size);

    return size;
}

fuse_operations operations(char const* fileName) {
    DomParser::parse(fileName, fs);
    return fuse_operations{
        .getattr = getattr,
        .open = open,
        .read = read,
        .readdir = readdir,
        .init = init,
    };
}
