// file      : dom-fs/dom-fs.cpp
// copyright : Copyright (c) 2019 Alexander Gnatyuk
// license   : GPLv2; see accompanying LICENSE file

import std.core;
import std.io;
import FuseDriver;

using std::cerr;
using std::exception;

using FuseWrapper::fuse_args;
using FuseWrapper::fuse_lib_help;
using FuseWrapper::fuse_main_loop;
using FuseWrapper::fuse_operations;

int main(int argc, char* argv[]) {
    try {
        Options options;
        fuse_args args{argc, argv, 0};

        if (fuse_opt_parse(&args, &options, option_spec, nullptr) == -1) {
            return 1;
        }

        if (options.showHelp) {
            fuse_lib_help(&args);
            return 0;
        }

        fuse_operations oper = operations(options.fileName);
        int ret = fuse_main_loop(args.argc, args.argv, &oper, nullptr);
        fuse_opt_free_args(&args);
        return ret;
    } catch (exception const& e) {
        cerr << "Exception occurred: " << e.what() << '\n';
        return 1;
    } catch (...) {
        cerr << "Unknown exception occurred\n";
        return 1;
    }
}
